﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using IART_proj.BaseStructures.Exceptions;

namespace IART_proj.BaseStructures.MapStructures
{
    /// <summary>
    /// Provides functions to read and write maps in XML format.
    /// </summary>
    class MapManager
    {
        public City ActiveCity { get; set; }

        public MapManager()
        {
        }

        /// <summary>
        /// Loads a map from a xml file.
        /// </summary>
        /// <param name="filename">The filename of the xml document.</param>
        /// <returns></returns>
        public City LoadMap(string filename)
        {
            try
            {
                var document = new XmlDocument();
                document.Load(filename);
                var cityNodeList = document.GetElementsByTagName("city");
                if (cityNodeList.Equals(null)) return null;
                for (var j = 0; j < cityNodeList.Count; j++)
                {
                    var cityAttributes = cityNodeList[j].Attributes;
                    if (cityAttributes == null) continue;
                    var capacity = Convert.ToInt32(cityAttributes.GetNamedItem("capacity").InnerText);
                    var fuel = Convert.ToInt32(cityAttributes.GetNamedItem("fuel").InnerText);
                    var ambStNode = Convert.ToInt32(cityAttributes.GetNamedItem("stNode").InnerText);
                    var active = cityAttributes.GetNamedItem("active").InnerText;

                    var nodeNodeList = document.GetElementsByTagName("node");
                    var nodes = ParseNodes(nodeNodeList);
                    var edgeNodeList = document.GetElementsByTagName("edge");
                    var edges = ParseEdges(edgeNodeList, nodes);
                    var patientNodeList = document.GetElementsByTagName("patient");
                    var patients = ParsePatients(patientNodeList, nodes);
                    var city = new City(nodes, edges, patients,
                        new Ambulance(fuel, capacity, nodes.Find(n => n.Id == ambStNode),fuel));
                    
                    ActiveCity = city;
                }
                
                return ActiveCity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private static List<Node> ParseNodes(XmlNodeList nodeNodeList)
        {
            var nodes = new List<Node>();
            var nUsedIds = new List<int>();
            for (var i = 0; i < nodeNodeList.Count; i++)
            {
                var attributes = nodeNodeList[i].Attributes;
                if (attributes == null) continue;
                var name = attributes.GetNamedItem("name").InnerText;
                var nodeId = Convert.ToInt32(attributes.GetNamedItem("id").InnerText);
                var type = attributes.GetNamedItem("type").InnerText;
                var x = Convert.ToInt32(attributes.GetNamedItem("x").InnerText);
                var y = Convert.ToInt32(attributes.GetNamedItem("y").InnerText);
                var cap = attributes.GetNamedItem("capacity");
                var capacity = -1;
                if (cap != null)
                {
                    capacity = Convert.ToUInt16(cap.InnerText);
                }
                    
                foreach (var id in nUsedIds.Where(id => id == nodeId))
                {
                    throw new RepeatedIdException("Patient", id);
                }
                nUsedIds.Add(nodeId);

                var t = Node.NodeType.REGULAR;

                if (type == "hospital")
                    t = Node.NodeType.HOSPITAL;
                else if (type == "pump")
                    t = Node.NodeType.PUMP;
                var node = capacity >= 0 ? new Node(name, nodeId, t, x, y, capacity) : new Node(name, nodeId, t, x, y, int.MaxValue);
                nodes.Add(node);
            }
            return nodes;
        }

        static private List<Road> ParseEdges(XmlNodeList edgeNodeList, List<Node> nodes)
        {
            var edges = new List<Road>();
            for (var i = 0; i < edgeNodeList.Count; i++)
            {
                var attributes = edgeNodeList[i].Attributes;
                if (attributes == null) continue;
                var edgeStNode = Convert.ToInt32(attributes.GetNamedItem("stNode").InnerText);
                var edgeEndNode = Convert.ToInt32(attributes.GetNamedItem("endNode").InnerText);
                var bidirectional = attributes.GetNamedItem("bidirectional").InnerText != "false";
                
                var node1 = nodes.Find(n => n.Id == edgeStNode);
                if (node1 == null)
                    throw new InvalidIdReferenceException("stNode", edgeStNode);
                var node2 = nodes.Find(n => n.Id == edgeEndNode);
                if (node2 == null)
                    throw new InvalidIdReferenceException("endNode", edgeEndNode);
                
                var weight = Node.GetDistance(node1, node2);
                var edge = new Road(weight, node1, node2, bidirectional);
                
                edges.Add(edge);
            }
            return edges;
        }

        private static List<Patient> ParsePatients(XmlNodeList patientNodeList, List<Node> nodes)
        {
            var patients = new List<Patient>();
            var pUsedIds = new List<int>();
            for (var i = 0; i < patientNodeList.Count; i++)
            {
                var attributes = patientNodeList[i].Attributes;
                if (attributes == null) continue;
                var patientOriginNode = Convert.ToInt32(attributes.GetNamedItem("originNode").InnerText);
                var patientTargetNode = Convert.ToInt32(attributes.GetNamedItem("targetNode").InnerText);
                var patientId = Convert.ToInt32(attributes.GetNamedItem("id").InnerText);

                foreach (var id in pUsedIds.Where(id => id == patientId))
                {
                    throw new RepeatedIdException("Patient", id);
                }
                pUsedIds.Add(patientId);

                var node1 = nodes.Find(n => n.Id == patientOriginNode);
                if (node1 == null)
                    throw new InvalidIdReferenceException("originNode", patientOriginNode);

                var node2 = nodes.Find(n => n.Id == patientTargetNode);
                if (node2 == null)
                    throw new InvalidIdReferenceException("originNode", patientTargetNode);

                var patient = new Patient(patientId, node1, node2);
                patients.Add(patient);
            }

            return patients;
        }

        /// <summary>
        /// Saves a map to a xml file.
        /// </summary>
        /// <param name="filename">The filename of the xml document.</param>
        /// <param name="mapToSave">The map to be saved.</param>
        public void SaveMap(string filename, City mapToSave)
        {
            using (var file = new StreamWriter(filename))
            {
                
                file.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                file.WriteLine("<city capacity=\"" +  mapToSave.ActiveAmbulance.Capacity + "\" fuel=\"" + mapToSave.ActiveAmbulance.FuelCapaciy + "\" stNode=\"" + mapToSave.ActiveAmbulance.CurrentNode.Id + "\" active=\"true\">");
                #region Write nodes
                file.WriteLine("    <nodes>");
                foreach (var node in mapToSave.Nodes)
                {
                    string type;
                    switch (node.Type)
                    {
                        case Node.NodeType.HOSPITAL:
                            type = "hospital";
                            break;
                        case Node.NodeType.PUMP:
                            type = "pump";
                            break;
                        default:
                            type = "regular";
                            break;
                    }

                    string toWrite = "      <node name=\"" + node.Name + "\" id=\"" + node.Id + "\" type=\"" + type +
                                     "\" x=\"" + node.Coords.X + "\" y=\"" + node.Coords.Y + "\"";
                    if (node.Type == Node.NodeType.HOSPITAL)
                        toWrite += " capacity=\"" + node.Capacity + "\"/>";
                    else
                    {
                        toWrite += "/>";
                    }
                    file.WriteLine(toWrite);
                }
                file.WriteLine("    </nodes>");
                #endregion
                #region Write edges
                file.WriteLine("    <edges>");
                foreach (var edge in mapToSave.Edges)
                {
                    var bidirectional = "true";
                    if (!edge.IsBidirectional)
                        bidirectional = "false";
                    var toWrite = "      <edge stNode=\"" + edge.Node1.Id + "\" endNode=\"" + edge.Node2.Id + "\" bidirectional=\"" + bidirectional + "\"/>";
                    
                    file.WriteLine(toWrite);
                }
                file.WriteLine("    </edges>");
                #endregion
                #region Write patients
                file.WriteLine("    <patients>");
                foreach (var patient in mapToSave.Patients)
                {
                    var toWrite = "      <patient id=\"" + patient.PatientId + "\" originNode=\"" + patient.Location.Id + "\" targetNode=\"" + patient.Target.Id + "\"/>";

                    file.WriteLine(toWrite);
                }
                file.WriteLine("    </patients>");
                #endregion
                file.WriteLine("</city>");
            }
        }

        /// <summary>
        /// Removes a map from memory.
        /// </summary>
        /// <param name="mapToUnload">Map to remove.</param>
        public void UnloadMap(City mapToUnload)
        {
            ActiveCity = null;
        }
    }
}
