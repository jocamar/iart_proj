﻿using System.Collections.Generic;
using IART_proj.BaseStructures.Algorithms;
using Microsoft.Xna.Framework;

namespace IART_proj.BaseStructures.MapStructures
{
    class Ambulance
    {
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Coords.GetHashCode();
                hashCode = (hashCode*397) ^ Capacity;
                hashCode = (hashCode*397) ^ Fuel.GetHashCode();
                hashCode = (hashCode*397) ^ FuelCapaciy.GetHashCode();
                hashCode = (hashCode*397) ^ (CurrentNode != null ? CurrentNode.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static float Speed = 100;
        public int Capacity { get; set; }
        public float Fuel { get; set; }
        public float FuelCapaciy { get; set; }
        public Vector2 Coords;
        public Node CurrentNode { get; set; }

        /// <summary>
        /// Details the path calculated for this ambulance to pickup the patients.
        /// </summary>
        public List<Transition> Path { get; private set; }

        /// <summary>
        /// Default constructor for Ambulance. Throws InvalidStartNodeException if the start node is of an invalid type.
        /// </summary>
        /// <param name="fuel">The current fuel.</param>
        /// <param name="capacity">The capacity of the ambulance to transport patients.</param>
        /// <param name="currNode">The current node. Must be of type StartNodeType.</param>
        /// <param name="fuelCapacity">The maximum fuel capacity.</param>
        public Ambulance(float fuelCapacity, int capacity, Node currNode, float fuel)
        {
            this.Fuel = fuel;
            this.Capacity = capacity;
            this.CurrentNode = currNode;
            this.Path = new List<Transition>();
            this.Coords.X = currNode.Coords.X;
            this.Coords.Y = currNode.Coords.Y;
            this.FuelCapaciy = fuelCapacity;
        }

        protected bool Equals(Ambulance amb)
        {
            return Coords.Equals(amb.Coords) && Capacity == amb.Capacity && Fuel.Equals(amb.Fuel) && FuelCapaciy.Equals(amb.FuelCapaciy) && Equals(CurrentNode, amb.CurrentNode);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Ambulance) obj);
        }
    }
}
