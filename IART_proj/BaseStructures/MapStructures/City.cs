﻿using System.Collections.Generic;
using System.Linq;
using IART_proj.BaseStructures.Algorithms;

namespace IART_proj.BaseStructures.MapStructures
{
    /// <summary>
    /// Holds a set of nodes, edges, patients and ambulance that define a city
    /// </summary>
    class City
    {
        public List<Node> Nodes { get; private set; } 
        public List<Road> Edges { get; private set; }
        public List<Patient> Patients { get; private set; } 
        public Ambulance ActiveAmbulance { get; private set; }

        public City(List<Node> nodes, List<Road> edges, List<Patient> patients, Ambulance ambulance )
        {
            this.Edges = edges;
            this.Nodes = nodes;
            this.Patients = patients;
            this.ActiveAmbulance = ambulance;
        }

        public State GetStartingState()
        {
            var newAmb = new Ambulance(ActiveAmbulance.FuelCapaciy, ActiveAmbulance.Capacity,
                ActiveAmbulance.CurrentNode, ActiveAmbulance.Fuel);

            var newPatients = Patients.Select(patient => new Patient(patient.PatientId, patient.Location, patient.Target, patient.IsPickedUp, patient.IsDroppedOff)).ToList();

            return new State(null,newAmb,newPatients,this,0);
        }

        public void RecalculateEdgeWeights()
        {
            foreach (var edge in Edges)
            {
                edge.Weight = Node.GetDistance(edge.Node2, edge.Node1);
            }
        }


    }
}
