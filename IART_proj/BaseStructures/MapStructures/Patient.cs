﻿using System;
using IART_proj.BaseStructures.Exceptions;

namespace IART_proj.BaseStructures.MapStructures
{
    class Patient
    {
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = PatientId;
                hashCode = (hashCode*397) ^ (Target != null ? Target.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Location != null ? Location.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ IsPickedUp.GetHashCode();
                hashCode = (hashCode*397) ^ IsDroppedOff.GetHashCode();
                return hashCode;
            }
        }

        public readonly int PatientId;
        public Node Target { get; private set; }
        public Node Location { get; private set; }

        public bool IsPickedUp { get; set; }
        public bool IsDroppedOff { get; set; }
        public Node DroppedIn { get; set; }


        /// <summary>
        /// Default constructor for Patient. Throws InvalidTargetNodeException if destination.Type is different from TargetType.
        /// </summary>
        /// <param name="pId">The unique id of the patient.</param>
        /// <param name="location">The location of the patient.</param>
        /// <param name="destination">The destination of the patient.</param>
        /// <param name="pickedUp">Boolean indicating if the patient has been picked up by an ambulance.</param>
        /// <param name="droppedOff">Boolean indicating it the patient has been dropped off at his destination.</param>
        public Patient(int pId, Node location, Node destination, bool pickedUp = false, bool droppedOff = false, Node droppedIn = null)
        {
            this.PatientId = pId;
            this.DroppedIn = droppedIn;

            if (location == null) throw new ArgumentNullException("location");
            if (destination == null) throw new ArgumentNullException("destination");

            this.Target = destination;
            this.Location = location;
            this.IsPickedUp = pickedUp;
            this.IsDroppedOff = droppedOff;
            
            if(Target.Type != Node.NodeType.HOSPITAL)
                throw new InvalidTargetNodeException();
        }

        protected bool Equals(Patient p)
        {
            return PatientId == p.PatientId && Equals(Target, p.Target) && Equals(Location, p.Location) && IsPickedUp.Equals(p.IsPickedUp) && IsDroppedOff.Equals(p.IsDroppedOff);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Patient) obj);
        }
    }
}
