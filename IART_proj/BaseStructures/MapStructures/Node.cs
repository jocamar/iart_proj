﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework;

namespace IART_proj.BaseStructures.MapStructures
{
    /// <summary>
    /// Defines a node in the city for the ambulance to traverse. Each node represents a point of interest in
    /// the city (a cross-roads, t-intersection, gas pump, etc), and it has a number of roads coming to and 
    /// leaving, connecting it to other nodes.
    /// </summary>
    class Node
    {
        public enum NodeType
        {
            REGULAR,
            HOSPITAL,
            PUMP
        };
        public readonly NodeType Type;
        public readonly int Capacity;

        public string Name { get; private set; }
        public int Id { get; private set; }
 
        private readonly List<Road> roads  = new List<Road>();
        public ReadOnlyCollection<Road> Roads { get { return roads.AsReadOnly(); } }

        public Point Coords;

        /// <summary>
        /// Creates a new Node. Throws InvalidNodeTypeException if the node type hasn't been defined.
        /// </summary>
        /// <param name="name">A name that identifies this node.</param>
        /// <param name="id">An ID that identifies this node.</param>
        /// <param name="type">The type of the node, must have been declared as a valid type.</param>
        /// <param name="x">The position in the x axis.</param>
        /// <param name="y">The position in the y axis.</param>
        public Node(string name, int id, NodeType type, int x, int y, int capacity)
        {
            Name = name;
            Id = id;
            this.Type = type;
            this.Capacity = capacity;

            Coords.X = x;
            Coords.Y = y;
        }

        /// <summary>
        /// Attempts to add an edge to a node, throws InvalidRoadException if the road can't be added to the given connection.
        /// </summary>
        /// <param name="e">The edge to add.</param>
        public void AddEdge(Road e)
        {
            roads.Add(e);
        }

        public void RemoveEdge(Road e)
        {
            roads.Remove(e);
        }


        public static float GetDistance(Node n1, Node n2)
        {
            var rtrnval = (float)Math.Sqrt((n1.Coords.X - n2.Coords.X) * (n1.Coords.X - n2.Coords.X) + (n1.Coords.Y - n2.Coords.Y) * (n1.Coords.Y - n2.Coords.Y));
            return rtrnval;
        }
    }
}
