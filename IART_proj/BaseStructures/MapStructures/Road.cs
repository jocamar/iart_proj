﻿namespace IART_proj.BaseStructures.MapStructures
{
    class Road
    {
        public float Weight { get; set;}
        public bool IsBidirectional { get; private set; }
        public Node Node1 { get; private set; }
        public Node Node2 { get; private set; }

        /// <summary>
        /// Basic constructor for Road. Throws InvalidRoadException if it's not possible to assign the given road to the two connections.
        /// </summary>
        /// <param name="weight">Weight of the edge.</param>
        /// <param name="node1">Origin node.</param>
        /// <param name="node2">Destination node.</param>
        /// <param name="bidirectional">Defines wether the edge is bidirectional.</param>
        public Road(float weight, Node node1, Node node2, bool bidirectional)
        {
            this.Weight = weight;
            this.IsBidirectional = bidirectional;
            this.Node1 = node1;
            this.Node2 = node2;

            node1.AddEdge(this);
            node2.AddEdge(this);
        }

        /// <summary>
        /// Second constructor for Road. Throws InvalidRoadException if it's not possible to assign the given road to the origin connection.
        /// </summary>
        /// <param name="weight">Weight of the edge.</param>
        /// <param name="node">The origin node.</param>
        /// <param name="bidirectional">Defines whether the edge is bidirectional.</param>
        public Road(float weight, Node node, bool bidirectional)
        {
            this.Weight = weight;
            this.IsBidirectional = bidirectional;

            node.AddEdge(this);
        }
    }
}
