﻿using System;
using System.Collections.Generic;
using System.Linq;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    /// <summary>
    /// Defines a state as understood by the pathfinding algorithms.
    /// </summary>
    class State
    {
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (ActiveAmbulance != null ? ActiveAmbulance.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Patients != null ? Patients.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ActiveCity != null ? ActiveCity.GetHashCode() : 0);
                return hashCode;
            }
        }

        public readonly Ambulance ActiveAmbulance;
        public readonly List<Patient> Patients;
        public readonly City ActiveCity;
        public Transition Origin { get; set; }
        
        public float GScore { get; set; }


        /// <summary>
        /// Default constructor for State.
        /// </summary>
        /// <param name="origin">The transition that originated this state.</param>
        /// <param name="ambulance">The active ambulance.</param>
        /// <param name="patients">A list containing the patients still waiting for the ambulance in the city.</param>
        /// <param name="c">City containing the node map, won't be altered</param>
        /// <param name="cost">The minimum cost to get to the state from the starting state.</param>
        public State(Transition origin, Ambulance ambulance, List<Patient> patients, City c, float cost)
        {
            this.Origin = origin;
            this.ActiveAmbulance = ambulance;
            this.Patients = patients;
            this.ActiveCity = c;
            GScore = cost;
        }

        /// <summary>
        /// Returns the possible transitions from this state.
        /// </summary>
        public IEnumerable<Transition> GetTransitions(bool anyHospital)
        {
            var transitions = new List<Transition>();

            #region Check edge transitions
            foreach (var road in ActiveAmbulance.CurrentNode.Roads) 
            {
                if (road.IsBidirectional || road.Node1 == ActiveAmbulance.CurrentNode)
                {
                    if (ActiveAmbulance.Fuel > road.Weight)
                        transitions.Add(new FollowEdge(road, ActiveAmbulance.CurrentNode, this));
                }
            }
            #endregion

            #region Check drop patients transition
            if (ActiveAmbulance.CurrentNode.Type == Node.NodeType.HOSPITAL)
            {
                var patientsAtNode = Patients.Count(patient => patient.IsDroppedOff && patient.DroppedIn == ActiveAmbulance.CurrentNode);
                var patientsAtTarget = Patients.Count(patient => patient.IsPickedUp && !patient.IsDroppedOff && patient.Target == ActiveAmbulance.CurrentNode);
                var patientsNotDropped = Patients.Count(patient => patient.IsPickedUp && !patient.IsDroppedOff);
                if ((!anyHospital && patientsAtTarget > 0) || (anyHospital && patientsNotDropped > 0 && patientsAtNode < ActiveAmbulance.CurrentNode.Capacity))
                {
                    transitions.Add(new DropOff(ActiveAmbulance.CurrentNode, this));
                }
            }
            #endregion

            #region Check pickup patient transitions
            var pickedUpPatients = Patients.Count(patient => patient.IsPickedUp && !patient.IsDroppedOff);
            if (pickedUpPatients < ActiveAmbulance.Capacity)
            {
                transitions.AddRange((from patient in Patients
                                      where (patient.Location == ActiveAmbulance.CurrentNode && !patient.IsPickedUp && !patient.IsDroppedOff)
                                      select new PickUp(ActiveAmbulance.CurrentNode, patient.PatientId, this)));
            }
            #endregion

            #region Check refuel transitions
            if (ActiveAmbulance.CurrentNode.Type == Node.NodeType.PUMP && ActiveAmbulance.Fuel < ActiveAmbulance.FuelCapaciy)
            {
                transitions.Add(new Refuel(ActiveAmbulance.CurrentNode, this));
            }
            #endregion

            return transitions; 
        }

        /// <summary>
        /// Returns the calculated heuristic cost from this state to the goal state
        /// </summary>
        /// <returns></returns>
        public static float HeuristicFunction(State state, bool anyHospital, Dictionary<Node, Dictionary<Node, float>> distances = null)
        {
            #region Initialize variables
            float cost = 0;
            
            var patientsLeft = new List<Patient>();
            var patientsTaken = new List<Patient>();

            foreach (var patient in state.Patients)
            {
                if(!patient.IsDroppedOff && patient.IsPickedUp)
                    patientsTaken.Add(patient);
                if (!patient.IsPickedUp && !patient.IsDroppedOff)
                    patientsLeft.Add(patient);
            }

            var hospitalNodes = state.ActiveCity.Nodes.Where(node => node.Type == Node.NodeType.HOSPITAL).ToList();
            var patientsInNode = hospitalNodes.Select(t => state.Patients.Count(patient => patient.IsDroppedOff && patient.DroppedIn == t)).ToList();
            #endregion

            var currNode = state.ActiveAmbulance.CurrentNode;
            while (patientsLeft.Any() || patientsTaken.Any())
            {
                var minimumDist = float.MaxValue;
                #region If there are any patients to take and the ambulance has spots, go to the next closest patient
                if (patientsTaken.Count < state.ActiveAmbulance.Capacity && patientsLeft.Any())
                {
                    Patient closest = null;
                    foreach (var patient in patientsLeft)
                    {
                        if (closest == null)
                        {
                            closest = patient;
                            if (distances != null)
                                minimumDist = distances[closest.Location][currNode];
                            else
                                minimumDist = Node.GetDistance(closest.Location, currNode);
                        }
                        else
                        {
                            float dist;
                            if (distances != null)
                                dist = distances[patient.Location][currNode];
                            else
                                dist = Node.GetDistance(patient.Location, currNode);

                            if (!(dist < minimumDist)) continue;
                            minimumDist = dist;
                            closest = patient;
                        }
                    }
                    if (closest == null)
                        return float.MaxValue;
                    currNode = closest.Location;
                    patientsLeft.Remove(closest);
                    patientsTaken.Add(closest);
                    cost += minimumDist;
                }
                #endregion
                #region Go to a hospital
                else
                {
                    Node closest = null;
                    #region If the patients can't go to any hospital go to the closest hospital where one of the patients in the ambulance must be taken
                    if (!anyHospital)
                    {
                        foreach (var patient in patientsTaken)
                        {
                            if (closest == null)
                            {
                                closest = patient.Target;
                                if (distances != null)
                                    minimumDist = distances[closest][currNode];
                                else
                                    minimumDist = Node.GetDistance(closest, currNode);
                            }
                            else
                            {
                                float dist;
                                if (distances != null)
                                    dist = distances[patient.Target][currNode];
                                else
                                    dist = Node.GetDistance(patient.Target, currNode);
                                if (!(dist < minimumDist)) continue;
                                minimumDist = dist;
                                closest = patient.Target;
                            }
                        }

                        currNode = closest;
                        if (currNode == null)
                            return float.MaxValue;
                        for (var i = 0; i < patientsTaken.Count;)
                        {
                            if (patientsTaken[i].Target == currNode)
                                patientsTaken.RemoveAt(i);
                            else i++;
                        }
                        cost += minimumDist;
                    }
                    #endregion
                    #region Otherwise go to the closest one
                    else
                    {
                        var closestNodeIndex = 0;
                        for (var i = 0; i < hospitalNodes.Count; i++)
                        {
                            var node = hospitalNodes[i];
                            if (closest == null && patientsInNode[i] < node.Capacity)
                            {
                                closest = node;
                                if (distances != null)
                                    minimumDist = distances[closest][currNode];
                                else
                                    minimumDist = Node.GetDistance(closest, currNode);
                                closestNodeIndex = i;
                            }
                            else
                            {
                                float dist;
                                if (distances != null)
                                    dist = distances[node][currNode];
                                else
                                    dist = Node.GetDistance(node, currNode);

                                if (!(dist < minimumDist) || patientsInNode[i] >= node.Capacity) continue;
                                minimumDist = dist;
                                closest = node;
                                closestNodeIndex = i;
                            }
                        }
                        currNode = closest;
                        if (currNode == null)
                            return float.MaxValue;
                        for (var i = 0; i < patientsTaken.Count;)
                        {
                            if (patientsInNode[closestNodeIndex] < currNode.Capacity)
                            {
                                patientsTaken.Remove(patientsTaken[i]);
                                patientsInNode[closestNodeIndex]++;
                            }
                            else i++;
                        }

                        cost += minimumDist;
                    }
                    #endregion
                }
                #endregion
            }
            return cost;
        }

        public static float HeuristicFunction2(State state, bool anyHospital, Dictionary<Node, Dictionary<Node, float>> distances = null)
        {
            return 0;
        }

        public static float HeuristicFunction3(State state, bool anyHospital, Dictionary<Node, Dictionary<Node, float>> distances = null)
        {
            #region Initialize variables
            float cost = 0;

            var patientsLeft = new List<Patient>();
            var patientsTaken = new List<Patient>();

            foreach (var patient in state.Patients)
            {
                if (!patient.IsDroppedOff && patient.IsPickedUp)
                    patientsTaken.Add(patient);
                if (!patient.IsPickedUp && !patient.IsDroppedOff)
                    patientsLeft.Add(patient);
            }

            var hospitalNodes = state.ActiveCity.Nodes.Where(node => node.Type == Node.NodeType.HOSPITAL).ToList();
            var patientsInNode = hospitalNodes.Select(t => state.Patients.Count(patient => patient.IsDroppedOff && patient.DroppedIn == t)).ToList();
            #endregion

            var currNode = state.ActiveAmbulance.CurrentNode;
            while (patientsLeft.Any() || patientsTaken.Any())
            {
                var minimumDist = float.MaxValue;
                #region If the ambulance has no patients go to the closest one
                if (patientsTaken.Count == 0 && patientsLeft.Any())
                {
                    Patient closest = null;
                    foreach (var patient in patientsLeft)
                    {
                        if (closest == null)
                        {
                            closest = patient;
                            if (distances != null)
                                minimumDist = distances[closest.Location][currNode];
                            else
                                minimumDist = Node.GetDistance(closest.Location, currNode);
                        }
                        else
                        {
                            float dist;
                            if (distances != null)
                                dist = distances[patient.Location][currNode];
                            else
                                dist = Node.GetDistance(patient.Location, currNode);

                            if (!(dist < minimumDist)) continue;
                            minimumDist = dist;
                            closest = patient;
                        }
                    }
                    if (closest == null)
                        return float.MaxValue;
                    currNode = closest.Location;
                    patientsLeft.Remove(closest);
                    patientsTaken.Add(closest);
                    cost += minimumDist;
                }
                #endregion
                #region Go to the closest among patients and target hospitals
                else
                {
                    Node closest = null;
                    Patient closestPatient = null;
                    bool closestIsHospital = true;
                    var closestNodeIndex = 0;
                    if (!anyHospital)
                    {
                        foreach (var patient in patientsTaken)
                        {
                            if (closest == null)
                            {
                                closest = patient.Target;
                                if (distances != null)
                                    minimumDist = distances[closest][currNode];
                                else
                                    minimumDist = Node.GetDistance(closest, currNode);
                            }
                            else
                            {
                                float dist;
                                if (distances != null)
                                    dist = distances[patient.Target][currNode];
                                else
                                    dist = Node.GetDistance(patient.Target, currNode);
                                if (!(dist < minimumDist)) continue;
                                minimumDist = dist;
                                closest = patient.Target;
                            }
                        }
                    }
                    else
                    {
                        for (var i = 0; i < hospitalNodes.Count; i++)
                        {
                            var node = hospitalNodes[i];
                            if (closest == null && patientsInNode[i] < node.Capacity)
                            {
                                closest = node;
                                if (distances != null)
                                    minimumDist = distances[closest][currNode];
                                else
                                    minimumDist = Node.GetDistance(closest, currNode);
                                closestNodeIndex = i;
                            }
                            else
                            {
                                float dist;
                                if (distances != null)
                                    dist = distances[node][currNode];
                                else
                                    dist = Node.GetDistance(node, currNode);

                                if (!(dist < minimumDist) || patientsInNode[i] >= node.Capacity) continue;
                                minimumDist = dist;
                                closest = node;
                                closestNodeIndex = i;
                            }
                        }
                    }

                    if (patientsTaken.Count < state.ActiveAmbulance.Capacity)
                    {
                        foreach (var patient in patientsLeft)
                        {
                            float dist;
                            if (closestPatient == null)
                            {
                                closestPatient = patient;
                                if (distances != null)
                                    dist = distances[closestPatient.Location][currNode];
                                else
                                    dist = Node.GetDistance(closestPatient.Location, currNode);
                            }
                            else
                            {
                                if (distances != null)
                                    dist = distances[patient.Location][currNode];
                                else
                                    dist = Node.GetDistance(patient.Location, currNode);
                            }

                            if (!(dist < minimumDist)) continue;
                            minimumDist = dist;
                            closestPatient = patient;
                            closestIsHospital = false;
                        }
                    }

                    if (closestIsHospital)
                        currNode = closest;
                    else if (closestPatient != null) currNode = closestPatient.Location;

                    if (currNode == null)
                        return float.MaxValue;

                    if (closestIsHospital)
                    {
                        if (!anyHospital)
                        {
                            for (var i = 0; i < patientsTaken.Count; )
                            {
                                if (patientsTaken[i].Target == currNode)
                                    patientsTaken.RemoveAt(i);
                                else i++;
                            }
                        }
                        else
                        {
                            for (var i = 0; i < patientsTaken.Count; )
                            {
                                if (patientsInNode[closestNodeIndex] < currNode.Capacity)
                                {
                                    patientsTaken.Remove(patientsTaken[i]);
                                    patientsInNode[closestNodeIndex]++;
                                }
                                else i++;
                            }
                        }
                    }
                    else
                    {
                        patientsLeft.Remove(closestPatient);
                        patientsTaken.Add(closestPatient);
                    }

                    cost += minimumDist;
                }
                #endregion
            }
            return cost;
        }

        public static float HeuristicFunction4(State state, bool anyHospital, Dictionary<Node, Dictionary<Node, float>> distances = null)
        {
            float cost = 0;

            var nodesWithPatients = new List<Node>();

            foreach(var patient in state.ActiveCity.Patients)
            {
                if (!patient.IsDroppedOff && !patient.IsPickedUp)
                {
                    if (!nodesWithPatients.Contains(patient.Location))
                    {
                        nodesWithPatients.Add(patient.Location);
                    }
                }
            }

            float min;
            foreach (var node in nodesWithPatients)
            {
                if (state.ActiveAmbulance.CurrentNode == node)
                    continue;

                min = float.MaxValue;
                foreach (var node2 in state.ActiveCity.Nodes.Where(node2 => node2 != node))
                {
                    if (distances != null)
                    {
                        if (distances[node][node2] < min)
                        {
                            min = distances[node][node2];
                        }
                    }
                    else
                    {
                        var dist = Node.GetDistance(node, node2);
                        if (dist < min)
                            min = dist;
                    }
                }

                cost += min;
            }

            min = float.MaxValue;
            if (nodesWithPatients.Any())
            {
                foreach (var node in state.ActiveCity.Nodes.Where(node => node.Type == Node.NodeType.HOSPITAL))
                {
                    var isTarget = false;

                    if (!anyHospital)
                    {
                        foreach (var patient in state.ActiveCity.Patients)
                        {
                            if (patient.Target == node && !nodesWithPatients.Contains(node))
                                isTarget = true;
                        }
                    }

                    if (anyHospital || isTarget)
                    {
                        foreach (var patientNode in nodesWithPatients)
                        {
                            if (distances != null)
                            {
                                if (distances[node][patientNode] < min)
                                    min = distances[node][patientNode];
                            }
                            else
                            {
                                var dist = Node.GetDistance(node, patientNode);
                                if (dist < min)
                                    min = dist;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var node in state.ActiveCity.Nodes.Where(node => node.Type == Node.NodeType.HOSPITAL))
                {
                    var isTarget = false;
                    if (!anyHospital)
                    {
                        foreach (var patient in state.ActiveCity.Patients)
                        {
                            if (patient.Target == node)
                                isTarget = true;
                        }
                    }

                    if (anyHospital || isTarget)
                    {
                        if (distances != null)
                        {
                            if (distances[node][state.ActiveAmbulance.CurrentNode] < min)
                                min = distances[node][state.ActiveAmbulance.CurrentNode];
                        }
                        else
                        {
                            var dist = Node.GetDistance(node, state.ActiveAmbulance.CurrentNode);
                            if (dist < min)
                                min = dist;
                        }
                    }
                }
            }

            cost += min;
            
            
            return cost;
        }
        public static float CostFunction(State state)
        {
            return state.GScore;
        }

        public static float CostFunction2(State state)
        {
            return 0;
        }

        protected bool Equals(State s)
        {
            return Equals(ActiveAmbulance, s.ActiveAmbulance) && Patients.SequenceEqual(s.Patients) && Equals(ActiveCity, s.ActiveCity);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((State) obj);
        }

        public List<Transition> GetPath()
        {
            var path = new List<Transition>();
            var current = this;

            while (current.Origin != null && current.Origin.OriginState != null)
            {
                path.Add(current.Origin);
                current = current.Origin.OriginState;
            }

            path.Reverse();
            return path;
        }

        public bool IsFinal()
        {
            return Patients.Count == 0 || Patients.All(patient => patient.IsDroppedOff);
        }
    }
}
