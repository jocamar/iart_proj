﻿using System;
using System.Collections.Generic;
using System.Linq;
using IART_proj.BaseStructures.MapStructures;
using IART_proj.BaseStructures.UI;

namespace IART_proj.BaseStructures.Algorithms
{
    abstract class RoutingSystem
    {
        protected readonly bool anyHospital;
        protected readonly State startingState;
        protected readonly SharedMemory mem;
        public TimeSpan Time { get; protected set; }
        

        protected RoutingSystem(SharedMemory mem, bool anyHospital)
        {
            this.mem = mem;
            this.startingState = mem.ActiveCity.GetStartingState();
            this.anyHospital = anyHospital;
        }

        public abstract List<Transition> CalculateRoute();
    }

    class AStarSystem : RoutingSystem
    {
        #region Properties and delegates
        private readonly bool preComputeDist;
        public delegate float CostFunction(State state);
        public delegate float HeuristicFunction(State state, bool anyHospital, Dictionary<Node, Dictionary<Node, float>> distances);

        private CostFunction costFunc;
        private HeuristicFunction hFunc;
        #endregion
        public AStarSystem(SharedMemory mem, CostFunction costFunc, HeuristicFunction hFunc, bool anyHospital = false, bool preComputeDist = true) : base(mem, anyHospital)
        {
            this.preComputeDist = preComputeDist;
            this.costFunc = costFunc;
            this.hFunc = hFunc;
        }

        private static Dictionary<Node, Dictionary<Node, float>> FloydWarshall(City city)
        {
            #region Populate distances matrix
            var distances = new Dictionary<Node, Dictionary<Node, float>>();
            foreach (var node in city.Nodes)
            {
                distances[node] = new Dictionary<Node, float>();

                foreach (var node2 in city.Nodes)
                {
                    if (node == node2)
                    {
                        distances[node][node2] = 0;
                        continue;
                    }
                        
                    var dist = node.Roads.Where(edge => edge.Node2 == node2 || (edge.IsBidirectional && edge.Node1 == node2)).Select(edge => edge.Weight).Concat(new[] {float.MaxValue}).Min();

                    distances[node][node2] = dist;
                }
            }
            #endregion

            #region Calculate distances
            for (var k = 0; k < city.Nodes.Count; k++)
            {
                var node_k = city.Nodes[k];
                for (var i = 0; i < city.Nodes.Count; i++)
                {
                    var node_i = city.Nodes[i];
                    for (var j = 0; j < city.Nodes.Count; j++)
                    {
                        var node_j = city.Nodes[j];
                        distances[node_i][node_j] = Math.Min(distances[node_i][node_j], distances[node_i][node_k] + distances[node_k][node_j]);
                    }
                }
            }
            #endregion

            return distances;
        } 

        public override List<Transition> CalculateRoute()
        {
            #region Initialize variables and precompute distances
            var begin = DateTime.Now;

            Dictionary<Node, Dictionary<Node, float>> dist = null;
            if (preComputeDist)
                dist = FloydWarshall(startingState.ActiveCity);
            lock (mem)
            {
                mem.Iteration = 0;
                mem.MemoryUsed = GC.GetTotalMemory(false);
            }
           
            
            DateTime end;
            var closedSet = new List<State>();
            var openSet = new PriorityQueue<float, State>();
            #endregion
           
            openSet.Enqueue(0,startingState);
            
            startingState.GScore = 0;
            while (openSet.Any())
            {
                #region Get the least cost neighbor and check if it's the goal

                lock (mem)
                {
                    mem.Iteration++;
                    mem.MemoryUsed = GC.GetTotalMemory(false);
                }
                
                var current = openSet.Dequeue();
                if (current.Value.IsFinal())
                {
                     end = DateTime.Now;
                    Time = end - begin;
                    return current.Value.GetPath();
                }
                closedSet.Add(current.Value);
                #endregion

                #region Find neighbors
                var neighbors = new List<State>();
                var transitions = current.Value.GetTransitions(anyHospital);
                foreach (var resulting in transitions.Select(t => t.GetResultingState(startingState.ActiveCity, anyHospital)).Where(resulting => !neighbors.Contains(resulting)))
                {
                    neighbors.Add(resulting);
                }
                #endregion

                #region Analyse neighbors and add them to the open set
                foreach (var neighbor in neighbors)
                {
                    var inOpenSet = false;
                    if (closedSet.Contains(neighbor))
                        continue;
                    
                    foreach (var state in openSet.Where(state => neighbor.Equals(state.Value)))
                    {
                        inOpenSet = true;
                        if (!(state.Value.GScore > current.Value.GScore + neighbor.Origin.GetWeight())) continue;
                        state.Value.Origin = neighbor.Origin;
                        state.Value.GScore = current.Value.GScore + neighbor.Origin.GetWeight();
                    }
                       
                    if(!inOpenSet)
                        openSet.Enqueue(costFunc(neighbor) + hFunc(neighbor,anyHospital, dist),neighbor);
                }
                #endregion
            }

            end = DateTime.Now;
            Time = end - begin;
            return null;
        }
    }
}
