﻿using System.Collections.Generic;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    class PickUp : Transition
    {
        public readonly int PatientId;
        public PickUp(Node originalNode, int pId, State state) : base(originalNode,originalNode, state)
        {
            this.PatientId = pId;
        }

        public override State GetResultingState(City city, bool anyHospital)
        {
            var newAmb = new Ambulance(OriginState.ActiveAmbulance.FuelCapaciy, OriginState.ActiveAmbulance.Capacity,
                OriginState.ActiveAmbulance.CurrentNode, OriginState.ActiveAmbulance.Fuel);

             var newPatients = new List<Patient>();
            foreach (var patient in OriginState.Patients)
            {
                if(patient.PatientId == PatientId)
                    newPatients.Add(new Patient(patient.PatientId,patient.Location,patient.Target,true));
                else
                    newPatients.Add(new Patient(patient.PatientId,patient.Location,patient.Target,patient.IsPickedUp,patient.IsDroppedOff,patient.DroppedIn));
            }
            return new State(this,newAmb,newPatients,city,OriginState.GScore);
        }

        public override string ToString()
        {
            return "Picked up patient with ID " + PatientId + " at " + DestinationNode.Name;
        }
    }
}
