﻿using System.Collections.Generic;
using System.Linq;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    class DropOff : Transition
    {
        public DropOff(Node node, State state) : base(node,node,state)
        {
        }

        public override State GetResultingState(City city, bool anyHospital)
        {
            var node = OriginState.ActiveAmbulance.CurrentNode;
            var newAmb = new Ambulance(OriginState.ActiveAmbulance.FuelCapaciy, OriginState.ActiveAmbulance.Capacity,
                node, OriginState.ActiveAmbulance.Fuel);

            var newPatients = new List<Patient>();
            var patientsDropped = 0;
            var patientsInHospital = OriginState.Patients.Count(patient => patient.IsDroppedOff && patient.DroppedIn == node);

            foreach (var patient in OriginState.Patients)
            {
                if (((!anyHospital && patient.Target == node) || (anyHospital && patientsDropped + patientsInHospital < node.Capacity)) &&
                    patient.IsPickedUp && !patient.IsDroppedOff)
                {
                    newPatients.Add(new Patient(patient.PatientId, patient.Location, patient.Target, false, true, node));
                    patientsDropped++;
                }
                else
                    newPatients.Add(new Patient(patient.PatientId,patient.Location,patient.Target,patient.IsPickedUp,patient.IsDroppedOff, patient.DroppedIn));
            }

            return new State(this,newAmb,newPatients,city, OriginState.GScore);
        }

        public override string ToString()
        {
            var node = OriginState.ActiveAmbulance.CurrentNode;

            return "Dropped patients at " + node.Name;
        }
    }
}
