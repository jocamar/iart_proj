﻿using System.Linq;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    /// <summary>
    /// Represents a transition that represents following an edge from a node to another (or possibly the same) node.
    /// </summary>
    class FollowEdge : Transition
    {
        //public readonly RoadConnection Edge;
        public readonly Road Edge;

        /// <summary>
        /// Default constructor for FollowEdge transition.
        /// </summary>
        /// <param name="originNode">The node where this transition originated.</param>
        /// <param name="state">The state that originated this transition.</param>
        /// <param name="road">The road to follow.</param>
        public FollowEdge(Road road, Node originNode, State state) : base(originNode, originNode, state)
        {
            this.Edge = road;

            if (originNode == Edge.Node1)
                DestinationNode = Edge.Node2;
            else
            {
                DestinationNode = Edge.Node1;
            }
        }

        public override State GetResultingState(City city, bool anyHospital)
        {

            var newAmb = new Ambulance(OriginState.ActiveAmbulance.FuelCapaciy, OriginState.ActiveAmbulance.Capacity,
                DestinationNode, (float) (OriginState.ActiveAmbulance.Fuel - Edge.Weight));

            var newPatients = OriginState.Patients.Select(patient => new Patient(patient.PatientId, patient.Location, patient.Target, patient.IsPickedUp, patient.IsDroppedOff,patient.DroppedIn)).ToList();

            return new State(this,newAmb,newPatients,city,OriginState.GScore+Edge.Weight);
        }

        public override float GetWeight()
        {
            return Edge.Weight;
        }

        public override string ToString()
        {
            return "Followed road to " + DestinationNode.Name + ", remaining fuel is " + (OriginState.ActiveAmbulance.Fuel - Edge.Weight);
        }
    }
}
