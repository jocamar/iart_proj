﻿using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    abstract class Transition
    {
        public Node OriginNode { get; protected set; }
        public Node DestinationNode { get; protected set; }
        public readonly State OriginState;

        protected Transition(Node originNode, Node destinationNode, State origin)
        {
            this.OriginNode = originNode;
            this.DestinationNode = destinationNode;
            this.OriginState = origin;
        }

        /// <summary>
        /// Returns the resulting state after this transition is followed.
        /// </summary>
        public abstract State GetResultingState(City c, bool anyHospital);

        public virtual float GetWeight()
        {
            return 0;
        }
    }
}
