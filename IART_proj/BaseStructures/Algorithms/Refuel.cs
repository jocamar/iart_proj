﻿using System.Linq;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.Algorithms
{
    /// <summary>
    /// Represents a transition that consists of refueling the active ambulance.
    /// </summary>
    class Refuel : Transition
    {
        public Refuel(Node originNode, State state) : base(originNode, originNode, state)
        {
        }

        public override State GetResultingState(City city, bool anyHospital)
        {
            var newAmb = new Ambulance(OriginState.ActiveAmbulance.FuelCapaciy, OriginState.ActiveAmbulance.Capacity,
                OriginState.ActiveAmbulance.CurrentNode, OriginState.ActiveAmbulance.FuelCapaciy);

            var newPatients = OriginState.Patients.Select(patient => new Patient(patient.PatientId, patient.Location, patient.Target, patient.IsPickedUp, patient.IsDroppedOff, patient.DroppedIn)).ToList();
            return new State(this,newAmb,newPatients,city,OriginState.GScore);
        }

        public override string ToString()
        {
            return "Refueled at " + DestinationNode.Name;
        }
    }
}
