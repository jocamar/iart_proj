﻿#region Using Statements

using System;
using IART_proj.BaseStructures.Algorithms;
using IART_proj.BaseStructures.MapStructures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

#endregion

namespace IART_proj.BaseStructures.UI
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    class PatientTransportSimulation : Game
    {
        #region Properties
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        
        private Texture2D bg; //just a placeholder, TODO change this later
        private Texture2D circle;
        private Texture2D road;
        private Texture2D arrow, arrow2;
        private SpriteFont font1;
        private const int WindowWidth = 720;
        private const int WindowHeight = 1280;
        private Vector3 _screenScale;

        private SharedMemory mem;
        #endregion

        public PatientTransportSimulation(SharedMemory mem)
            : base()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = WindowWidth,
                PreferredBackBufferWidth = WindowHeight
            };

            graphics.ApplyChanges();
            IsMouseVisible = true;
            Window.AllowUserResizing = false;

            Content.RootDirectory = "Content";

            this.mem = mem;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            bg = Content.Load<Texture2D>("ambulance");
            circle = Content.Load<Texture2D>("Circle");
            road = Content.Load<Texture2D>("pixel");
            font1 = Content.Load<SpriteFont>("MyFont");
            arrow = Content.Load<Texture2D>("Arrow");
            arrow2 = Content.Load<Texture2D>("Arrow2");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var scaleX = (float)GraphicsDevice.Viewport.Width / (float)WindowWidth;
            var scaleY = (float)GraphicsDevice.Viewport.Height / (float)WindowHeight;
            _screenScale = new Vector3(scaleX, scaleY, 1.0f);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape) || mem.Exit)
            {
                Exit();
            }
                
            lock (mem)
            {
                if (mem.Running && mem.RunningAmbulance != null && mem.Path != null)
                {
                    #region Go to the next edge transition
                    while (!(mem.Path[mem.CurrTransition] is FollowEdge))
                    {
                        if (mem.Path[mem.CurrTransition] is Refuel)
                        {
                            mem.RunningAmbulance.Fuel = mem.RunningAmbulance.FuelCapaciy;
                        }
                        if (mem.CurrTransition < mem.Path.Count - 1)
                        {
                            mem.CurrTransition++;
                        }
                        else
                        {
                            mem.Running = false;
                            return;
                        }
                    }
                    #endregion

                    #region Calculate nem ambulance position and fuel
                    var target = mem.Path[mem.CurrTransition].DestinationNode.Coords;
                    float dx = mem.RunningAmbulance.Coords.X - target.X;
                    float dy = mem.RunningAmbulance.Coords.Y - target.Y;

                    var dz = (float)Math.Sqrt(dx * dx + dy * dy);

                    var speedx = dx / dz * Ambulance.Speed;
                    var speedy = dy / dz * Ambulance.Speed;

                    mem.RunningAmbulance.Coords.X = (float)(mem.RunningAmbulance.Coords.X-speedx * (gameTime.ElapsedGameTime.Milliseconds / 1000.0));
                    mem.RunningAmbulance.Coords.Y = (float)(mem.RunningAmbulance.Coords.Y-speedy * (gameTime.ElapsedGameTime.Milliseconds / 1000.0));

                    mem.RunningAmbulance.Fuel -= (float)(Ambulance.Speed *(gameTime.ElapsedGameTime.Milliseconds / 1000.0));
                    #endregion

                    #region Check if the ambulance reached the next node
                    float newdx = mem.RunningAmbulance.Coords.X - target.X;
                    float newdy = mem.RunningAmbulance.Coords.Y - target.Y;

                    if (((newdx >= 0 && dx <= 0) || (newdx <= 0 && dx >= 0)) &&
                        ((newdy >= 0 && dy <= 0) || (newdy <= 0 && dy >= 0)))
                    {
                        mem.RunningAmbulance.Coords = new Vector2(target.X, target.Y);
                        
                           
                        if (mem.CurrTransition < mem.Path.Count - 1)
                        {
                            mem.CurrTransition++;
                        }
                        else
                        {
                            mem.Running = false;
                        }

                    }
                    #endregion
                }
            }
            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Lavender);

            try
            {
                var scaleMatrix = Matrix.CreateScale(_screenScale);
                spriteBatch.Begin();
                lock (mem)
                {
                    if (mem.ActiveCity != null)
                    {
                        #region Draw city
                        foreach (var edge in mem.ActiveCity.Edges)
                        {
                            DrawEdge(spriteBatch, edge, 2, Color.Black);
                        }
                        foreach (var node in mem.ActiveCity.Nodes)
                        {
                           Color color;
                            if (node.Type == Node.NodeType.PUMP)
                                color = Color.Blue;
                            else if (node.Type == Node.NodeType.HOSPITAL)
                                color = Color.Yellow;
                            else
                                color = Color.White;
                            spriteBatch.Draw(circle,
                                new Rectangle(node.Coords.X - circle.Width / 4, node.Coords.Y - circle.Height / 4,
                                    circle.Width / 2, circle.Height / 2), color);
                        }
                        foreach (var node in mem.ActiveCity.Nodes)
                        {
                            spriteBatch.DrawString(font1, node.Name,
                                new Vector2(node.Coords.X - circle.Width / 4 - 5, node.Coords.Y - circle.Height / 2), Color.Red);
                        }

                        spriteBatch.DrawString(font1, "Memory Usage: " + mem.MemoryUsed, new Vector2(30, 10),Color.Black);
                        spriteBatch.DrawString(font1, "States Visited: " + mem.Iteration, new Vector2(400, 10), Color.Black);
                        #endregion
                    }

                    if (mem.RunningAmbulance != null)
                    {
                        #region Draw ambulance and fuel bar
                        var fuelHeight = 100*mem.RunningAmbulance.Fuel/mem.RunningAmbulance.FuelCapaciy;
                        
                        spriteBatch.Draw(circle, new Rectangle((int)mem.RunningAmbulance.Coords.X - circle.Width / 8, (int)mem.RunningAmbulance.Coords.Y - circle.Height / 8,
                                    circle.Width / 4, circle.Height / 4), Color.Red);
                        spriteBatch.Draw(this.road, new Rectangle(1200, (int)(130 -fuelHeight), 50, (int)fuelHeight), Color.Red);
                        spriteBatch.DrawString(font1, "Fuel",
                                new Vector2(1205,130), Color.Black);
                        #endregion
                    }

                    spriteBatch.End();
                }
                
            }
            catch (Exception)
            {
                Console.WriteLine("\nThere has been an error while drawing the screen. Remember, only characters of the English alphabet are supported. Exiting.");
                Exit();
            }
            
            base.Draw(gameTime);
        }

        private void DrawEdge(SpriteBatch sb, Road road, int thickness, Color color)
        {
            #region Draw edge line
            var start = new Vector2(road.Node1.Coords.X, road.Node1.Coords.Y);
            var end = new Vector2(road.Node2.Coords.X, road.Node2.Coords.Y);
            
            var edge = end - start;
         
            // calculate angle to rotate line
            float angle =
                (float)Math.Atan2(edge.Y, edge.X);


            sb.Draw(this.road,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y-thickness/2,
                    (int)edge.Length(), //sb will strech the texture to fill this rectangle
                    thickness), //width of line, change this to make thicker line
                null,
                color, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0,(float)0.5), // point in line about which to rotate
                SpriteEffects.None,
                0);
            #endregion

            #region Draw arrows
            float x = start.X + (float)Math.Cos(angle) * (edge.Length() - circle.Width / 4 - arrow.Width);
            float y = start.Y + (float)Math.Sin(angle) * (edge.Length() - circle.Width / 4 - arrow.Width);

            sb.Draw(arrow,
                new Rectangle(
                    (int) x,
                    (int) y,
                    arrow.Width,
                    arrow.Height),
                null,
                color,
                angle,
                new Vector2(0, arrow.Height / 2), SpriteEffects.None,
                0);

            if (road.IsBidirectional)
            {
                x = start.X + (float)Math.Cos(angle) * (circle.Width / 4);
                y = start.Y + (float)Math.Sin(angle) * (circle.Width / 4);

                sb.Draw(arrow2,
                    new Rectangle(
                        (int)x,
                        (int)y,
                        arrow2.Width,
                        arrow2.Height),
                    null,
                    color,
                    angle,
                    new Vector2(0, arrow2.Height / 2), SpriteEffects.None,
                    0);
            }
            #endregion
        }
    }   
}
