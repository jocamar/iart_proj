﻿using System.Collections.Generic;
using IART_proj.BaseStructures.Algorithms;
using IART_proj.BaseStructures.MapStructures;

namespace IART_proj.BaseStructures.UI
{
    class SharedMemory
    {
        public bool Exit = false;
        public City ActiveCity = null;
        public List<Transition> Path = null;
        public bool Running = false;
        public Ambulance RunningAmbulance = null;
        public int CurrTransition = 0;
        public bool AnyHospital = false;
        public long MemoryUsed = 0;
        public int Iteration = 0;
    }
}
