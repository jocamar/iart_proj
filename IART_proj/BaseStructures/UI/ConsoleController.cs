﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IART_proj.BaseStructures.Algorithms;
using IART_proj.BaseStructures.MapStructures;
using Microsoft.Xna.Framework;

namespace IART_proj.BaseStructures.UI
{
    class ConsoleController
    {
        private MapManager mapManager;
        private SharedMemory mem = null;
        private bool done = false;

        private enum RouteFinder
        {
            ASTAR,
            STANDARD,
            GREEDY
        };

        public ConsoleController(SharedMemory mem)
        {
            this.mem = mem;
            mapManager = new MapManager();
            try
            {
                mapManager.LoadMap("content/Default.xml");
                lock (this.mem)
                {
                    if (mapManager.ActiveCity != null)
                    {
                        this.mem.ActiveCity = mapManager.ActiveCity;
                        this.mem.RunningAmbulance = new Ambulance(mem.ActiveCity.ActiveAmbulance.FuelCapaciy,
                        this.mem.ActiveCity.ActiveAmbulance.Capacity, mem.ActiveCity.ActiveAmbulance.CurrentNode,
                        this.mem.ActiveCity.ActiveAmbulance.Fuel);
                        this.mem.Path = null;
                        this.mem.Running = false;
                        this.mem.Iteration = 0;
                        this.mem.MemoryUsed = GC.GetTotalMemory(false);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not load default map.");
            }
            
        }

        private List<Transition> CalculateRoute(RouteFinder finder, bool anyHospital, bool preCompute, string heuristic)
        {
            if (mapManager == null || mapManager.ActiveCity == null)
            {
                Console.WriteLine("Error: Map not loaded.");
                return null;
            }

            List<Transition> path;
            switch (finder)
            {
                #region A-star
                case RouteFinder.ASTAR:
                    RoutingSystem routingAstar;
                    if(heuristic == "h1")
                        routingAstar = new AStarSystem(mem, State.CostFunction, State.HeuristicFunction3, anyHospital, preCompute);
                    else if(heuristic == "h2")
                        routingAstar = new AStarSystem(mem, State.CostFunction, State.HeuristicFunction4, anyHospital, preCompute);
                    else
                        routingAstar = new AStarSystem(mem, State.CostFunction, State.HeuristicFunction, anyHospital, preCompute);
                    

                    path = routingAstar.CalculateRoute();
                    if (path != null)
                    {
                        Console.WriteLine("---------------A* Solution--------------");
                        foreach (var t in path)
                        {
                            Console.WriteLine(t);
                        }

                        Console.WriteLine("---------------Analysis--------------");
                        Console.WriteLine("Took " + mem.Iteration + " steps to find a solution.");
                        Console.WriteLine("Time taken was: " + routingAstar.Time.Hours + "h" + routingAstar.Time.Minutes + "m" + routingAstar.Time.Seconds + "s" + routingAstar.Time.Milliseconds + "ms");
                        if (path.Count > 0)
                            Console.WriteLine("The final path cost is " + path[path.Count - 1].OriginState.GScore + ".\n\n\n\n");
                    }
                    else
                    {
                        Console.WriteLine("Unable to find a valid route. Sorry :(");
                        return null;
                    }
                    break;
                #endregion
                #region Greedy
                case RouteFinder.GREEDY:
                    RoutingSystem routingGreedy;
                    if(heuristic == "h1")
                        routingGreedy = new AStarSystem(mem, State.CostFunction2, State.HeuristicFunction3, anyHospital, preCompute);
                    else if (heuristic == "h2")
                        routingGreedy = new AStarSystem(mem, State.CostFunction2, State.HeuristicFunction4, anyHospital, preCompute);
                    else
                        routingGreedy = new AStarSystem(mem, State.CostFunction2, State.HeuristicFunction, anyHospital, preCompute);
                    
                    path = routingGreedy.CalculateRoute();
                    if (path != null)
                    {
                        Console.WriteLine("---------------Greedy Solution--------------");
                        foreach (var t in path)
                        {
                            Console.WriteLine(t);
                        }

                        Console.WriteLine("---------------Analysis--------------");
                        Console.WriteLine("Took " + mem.Iteration + " steps to find a solution.");
                        Console.WriteLine("Time taken was: " + routingGreedy.Time.Hours + "h" + routingGreedy.Time.Minutes + "m" + routingGreedy.Time.Seconds + "s" + routingGreedy.Time.Milliseconds + "ms");
                        if (path.Count > 0)
                            Console.WriteLine("The final path cost is " + path[path.Count - 1].OriginState.GScore + ".\n\n\n\n");
                    }
                    else
                    {
                        Console.WriteLine("Unable to find a valid route. Sorry :(");
                        return null;
                    }
                    break;
                #endregion
                #region Uniform cost
                default:
                    var routingStandard = new AStarSystem(mem, State.CostFunction, State.HeuristicFunction2, anyHospital, preCompute);
                    path = routingStandard.CalculateRoute();
                    if (path != null)
                    {
                        Console.WriteLine("---------------Standard Solution--------------");
                        foreach (var t in path)
                        {
                            Console.WriteLine(t);
                        }

                        Console.WriteLine("---------------Analysis--------------");
                        Console.WriteLine("Took " + mem.Iteration + " steps to find a solution.");
                        Console.WriteLine("Time taken was: " + routingStandard.Time.Hours + "h" + routingStandard.Time.Minutes + "m" + routingStandard.Time.Seconds + "s" + routingStandard.Time.Milliseconds + "ms");
                        if (path.Count > 0)
                            Console.WriteLine("The final path cost is " + path[path.Count - 1].OriginState.GScore + ".\n\n\n\n");
                    }
                    else
                    {
                        Console.WriteLine("Unable to find a valid route. Sorry :(");
                        return null;
                    }
                    break;
                #endregion
            }
            return path;
        }

        public void Run()
        {
            #region Initialization and start message
            
            Console.WriteLine("##########################################################");
            Console.WriteLine("#                                                        #");
            Console.WriteLine("#             Patient Transport Route Search             #");
            Console.WriteLine("#                    IART - 2013/2014                    #");
            Console.WriteLine("#                                                        #");
            Console.WriteLine("##########################################################");
            Console.WriteLine("Write a command and press 'enter' to execute it. You can");
            Console.WriteLine("find a list of valid commands by typing 'help'. Type 'quit'");
            Console.WriteLine("to exit the application.");
            #endregion

            while (!done)
            {
                #region Input reading
                Console.Write("RoutingSystem>");
                var command = "";
                var commandIssued = false;
                while (!commandIssued)
                {
                    try
                    {
                        command = Reader.ReadLine(200);
                        commandIssued = true;
                    }
                    catch
                    {

                    }
                }

                var args = Split(command,' ');
                if (args.Length < 1)
                    continue;
                #endregion
                var anyHospital = false;
                #region Input handling
                if (args[0] == "load" && args.Length >= 2)
                {
                    mapManager.LoadMap(args[1]);
                    lock (mem)
                    {
                        if (mapManager.ActiveCity != null)
                        {
                            mem.ActiveCity = mapManager.ActiveCity;
                            mem.RunningAmbulance = new Ambulance(mem.ActiveCity.ActiveAmbulance.FuelCapaciy,
                            mem.ActiveCity.ActiveAmbulance.Capacity, mem.ActiveCity.ActiveAmbulance.CurrentNode,
                            mem.ActiveCity.ActiveAmbulance.Fuel);
                            mem.Path = null;
                            mem.Running = false;
                            mem.Iteration = 0;
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                    }
                }
                else if (args[0] == "calculate" && args.Length >= 2)
                {
                    var preCompute = false;
                    string heuristic = null;
                    if (args.Length >= 3)
                    {
                        if (args[2] == "-any")
                            anyHospital = true;
                        else if (args[2] == "-precompute")
                            preCompute = true;
                        else if (args[2] == "-h1")
                            heuristic = "h1";
                        else if (args[2] == "-h2")
                            heuristic = "h2";

                        if (args.Length >= 4)
                        {
                            if (args[3] == "-any")
                                anyHospital = true;
                            else if (args[3] == "-precompute")
                                preCompute = true;
                            else if(args[3] == "-h1")
                                heuristic = "h1";
                            else if (args[3] == "-h2")
                                heuristic = "h2";

                            if(args.Length >= 5)
                            {
                                if (args[4] == "-any")
                                    anyHospital = true;
                                else if (args[4] == "-precompute")
                                    preCompute = true;
                                else if (args[4] == "-h1")
                                    heuristic = "h1";
                                else if (args[4] == "-h2")
                                    heuristic = "h2";
                            }
                        }

                    }

                    List<Transition> path = null;
                    if (args[1] == "greedy")
                    {
                        Console.WriteLine("Calculating...");
                        path = this.CalculateRoute(RouteFinder.GREEDY, anyHospital, preCompute, heuristic);
                    }
                    else if (args[1] == "astar")
                    {
                        Console.WriteLine("Calculating...");
                        path = this.CalculateRoute(RouteFinder.ASTAR, anyHospital, preCompute, heuristic);
                    }
                    else if (args[1] == "standard")
                    {
                        Console.WriteLine("Calculating, this may take a while...");
                        path = this.CalculateRoute(RouteFinder.STANDARD, anyHospital, preCompute, heuristic);
                    }
                    else
                    {
                        Console.WriteLine("Invalid arguments, must be one of the following: astar, greedy, standard.");
                    }

                    if (path != null)
                    {
                        mem.Path = path;
                    }
                }
                else if (args[0] == "run")
                {
                    lock (mem)
                    {
                        if (mem.Path == null)
                        {
                            Console.WriteLine("\nMust first calculate a path.");
                            continue;
                        }
                        mem.Running = true;
                        mem.RunningAmbulance = new Ambulance(mem.ActiveCity.ActiveAmbulance.FuelCapaciy,
                            mem.ActiveCity.ActiveAmbulance.Capacity, mem.ActiveCity.ActiveAmbulance.CurrentNode,
                            mem.ActiveCity.ActiveAmbulance.Fuel);
                        mem.CurrTransition = 0;
                        mem.AnyHospital = anyHospital;
                        mem.MemoryUsed = GC.GetTotalMemory(false);
                    }
                }
                else if (args[0] == "quit")
                {
                    done = true;
                    mem.Exit = true;
                }
                else if (args[0] == "patients")
                {
                    if (mem.ActiveCity != null && mem.ActiveCity.Patients != null)
                    {
                        foreach (var patient in mem.ActiveCity.Patients)
                        {
                            Console.WriteLine("Patient at " + patient.Location.Name + " ==> " + patient.Target.Name + ".");
                        }
                    }
                }
                else if (args[0] == "add-node")
                {
                    if (args.Length >= 5)
                    {
                        lock (mem)
                        {
                            AddNode(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nIncorrect number of arguments.");
                    }
                    
                }
                else if (args[0] == "remove-node")
                {
                    if (args.Length >= 2)
                    {
                        lock (mem)
                        {
                            RemoveNode(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nIncorrect number of arguments.");
                    }
                }
                else if (args[0] == "move-node")
                {
                    lock (mem)
                    {
                        if (args.Length >= 4)
                        {
                            MoveNode(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "add-edge")
                {
                    lock (mem)
                    {
                        if (args.Length >= 3)
                        {
                            AddEdge(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "remove-edges")
                {
                    lock (mem)
                    {
                        if (args.Length >= 3)
                        {
                            RemoveEdges(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "fuel")
                {
                    lock (mem)
                    {
                        if (mem.ActiveCity != null)
                        {
                            Console.WriteLine("\nAmbulance fuel capacity is: " + mem.ActiveCity.ActiveAmbulance.FuelCapaciy);
                        }
                        else
                        {
                            Console.WriteLine("\nMust load a map first.");
                        }
                    }
                    
                }
                else if (args[0] == "capacity")
                {
                    lock (mem)
                    {
                        if (mem.ActiveCity != null && mem.ActiveCity.ActiveAmbulance != null)
                        {
                            Console.WriteLine("\nAmbulance patient capacity is: " + mem.ActiveCity.ActiveAmbulance.Capacity);
                        }
                        else
                        {
                            Console.WriteLine("\nMust load a map first.");
                        }
                    }

                }
                else if (args[0] == "set-capacity")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            SetCapacity(args);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }

                }
                else if (args[0] == "set-fuel")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            SetFuel(args);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }

                }
                else if (args[0] == "set-ambulance")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            SetAmbulance(args);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }

                }
                else if (args[0] == "add-patient")
                {
                    lock (mem)
                    {
                        if (args.Length >= 3)
                        {
                            AddPatient(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "remove-patients")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            RemovePatients(args);
                            mem.MemoryUsed = GC.GetTotalMemory(false);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "save")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            SaveMap(args);
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "set-speed")
                {
                    lock (mem)
                    {
                        if (args.Length >= 2)
                        {
                            float speed;
                            if (float.TryParse(args[1], out speed))
                            {
                                Ambulance.Speed = speed;
                            }
                            else Console.WriteLine("\nIncorrect arguments.");
                        }
                        else
                        {
                            Console.WriteLine("\nIncorrect number of arguments.");
                        }
                    }
                }
                else if (args[0] == "help")
                {
                    Console.WriteLine("These are the valid commands:");
                    Console.WriteLine("quit - exits the program;\n");
                    Console.WriteLine("load <map-name> - loads a xml file defining the map;\n");
                    Console.WriteLine("calculate <greedy/astar/standard> - calculates a route using the specified\nalgorithm, -any, -precompute -h1 and -h2 can be used to allow patients to be\ntaken to any hospital, precompute distances and use an alternative heuristic respectively;\n");
                    Console.WriteLine("run - runs the animation showing the path;\n");
                    Console.WriteLine("patients - shows a list of the map patients and their locations and assigned\nhospitals;\n");
                    Console.WriteLine("add-node <name> <x> <y> <type> <capacity> - creates a new node at the specified\nlocation, defining capacity is optional;\n");
                    Console.WriteLine("remove-node <name> - removes a node and all its edges;\n");
                    Console.WriteLine("move-node <name> <x> <y> - moves a node to the specified coordinates;\n");
                    Console.WriteLine("add-edge <node1> <node2> - adds a directed edge from node1 to node2;\n");
                    Console.WriteLine("add-edge <node1> <node2> -bi - adds a bidirectional edge from node1 to node2;\n");
                    Console.WriteLine("remove-edges <node1> <node2> - removes all edges connecting node1 and node2;\n");
                    Console.WriteLine("fuel - Displays the current ambulance fuel capacity;\n");
                    Console.WriteLine("capacity - Displays the current ambulance patient capacity;\n");
                    Console.WriteLine("set-fuel <fuel> - sets the ambulance's fuel capacity to the specified value;\n");
                    Console.WriteLine("set-capacity <capacity> - sets the ambulance's patient capacity to the specified value;\n");
                    Console.WriteLine("set-ambulance <node> - sets the ambulance starting node;\n");
                    Console.WriteLine("add-patient <location> <target> - creates a new patient at location with the\nassigned hospital target;\n");
                    Console.WriteLine("remove-patients <location> - removes the patients at the specified location;\n");
                    Console.WriteLine("save <filename> - saves the map to a file;\n");
                    Console.WriteLine("set-speed <speed>- changes the animation speed;\n");
                }

                #endregion
            }
        }

        #region Helper functions
        private void SaveMap(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't save the map while the path animation is running.");
                    return;
                }

                mapManager.SaveMap(args[1] + ".xml", mem.ActiveCity);
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void RemovePatients(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't remove a patient while the path animation is running.");
                    return;
                }

                for (var i = 0; i < mem.ActiveCity.Patients.Count;)
                {
                    if (mem.ActiveCity.Patients[i].Location.Name == args[1])
                    {
                        mem.ActiveCity.Patients.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                mem.Running = false;
                mem.Path = null;
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void AddPatient(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't add a patient while the path animation is running.");
                    return;
                }

                var r = new Random();
                var id = r.Next();
                var contains = true;
                while (contains)
                {
                    var found = mem.ActiveCity.Patients.Any(patient => patient.PatientId == id);
                    if (!found)
                        contains = false;
                    else
                        id = r.Next();
                }

                Node location = null;
                Node target = null;
                foreach (var node in mem.ActiveCity.Nodes)
                {
                    if (node.Name == args[1])
                        location = node;

                    if (node.Name == args[2])
                        target = node;
                }

                if (location == null || target == null)
                {
                    Console.WriteLine("\nIncorrect location or target node.");
                    return;
                }
                else if (target.Type != Node.NodeType.HOSPITAL)
                {
                    Console.WriteLine("\nPatient target must be a hospital.");
                    return;
                }

                mem.ActiveCity.Patients.Add(new Patient(id, location, target));
                mem.Running = false;
                mem.Path = null;
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void SetAmbulance(string[] args)
        {
            if (mem.ActiveCity != null && mem.ActiveCity.ActiveAmbulance != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't change ambulance location while the path animation is running.");
                    return;
                }

                foreach (var node in mem.ActiveCity.Nodes.Where(node => node.Name == args[1]))
                {
                    mem.ActiveCity.ActiveAmbulance.CurrentNode = node;
                    mem.ActiveCity.ActiveAmbulance.Coords = new Vector2(node.Coords.X, node.Coords.Y);
                    mem.RunningAmbulance.Coords = new Vector2(node.Coords.X, node.Coords.Y);
                    mem.Running = false;
                    mem.Path = null;
                }
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void SetFuel(string[] args)
        {
            if (mem.ActiveCity != null && mem.ActiveCity.ActiveAmbulance != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't change fuel capacity while the path animation is running.");
                    return;
                }

                float fuel;
                if (!float.TryParse(args[1], out fuel))
                {
                    Console.WriteLine("\nInvalid fuel capacity.");
                    return;
                }
                mem.ActiveCity.ActiveAmbulance.FuelCapaciy = fuel;
                mem.ActiveCity.ActiveAmbulance.Fuel = fuel;
                mem.Running = false;
                mem.Path = null;
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void SetCapacity(string[] args)
        {
            if (mem.ActiveCity != null && mem.ActiveCity.ActiveAmbulance != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine(
                        "\nCan't change patient capacity while the path animation is running.");
                    return;
                }

                int capacity;
                if (!int.TryParse(args[1], out capacity))
                {
                    Console.WriteLine("\nInvalid patient capacity.");
                    return;
                }
                mem.ActiveCity.ActiveAmbulance.Capacity = capacity;
                mem.Running = false;
                mem.Path = null;
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void RemoveEdges(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine("\nCan't remove edges while the path animation is running.");
                    return;
                }

                foreach (var node in mem.ActiveCity.Nodes.Where(node => node.Name == args[1]))
                {
                    foreach (var node2 in mem.ActiveCity.Nodes.Where(node2 => node2.Name == args[2]))
                    {
                        for (var i = 0; i < node.Roads.Count;)
                        {
                            if (node.Roads[i].Node1 == node2 || node.Roads[i].Node2 == node2)
                            {
                                mem.ActiveCity.Edges.Remove(node.Roads[i]);
                                node2.RemoveEdge(node.Roads[i]);
                                node.RemoveEdge(node.Roads[i]);
                                mem.Running = false;
                                mem.Path = null;
                            }
                            else i++;
                        }
                        break;
                    }

                    break;
                }
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void AddEdge(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine("\nCan't add edge while the path animation is running.");
                    return;
                }

                var bidirectional = false;

                if (args.Length >= 4)
                {
                    if (args[3] == "-bi")
                        bidirectional = true;
                    else
                    {
                        Console.WriteLine("Invalid arguments.");
                        return;
                    }
                }
                foreach (var node in mem.ActiveCity.Nodes.Where(node => node.Name == args[1]))
                {
                    foreach (var node2 in mem.ActiveCity.Nodes.Where(node2 => node2.Name == args[2]))
                    {
                        mem.ActiveCity.Edges.Add(new Road(Node.GetDistance(node, node2), node, node2, bidirectional));
                        mem.Running = false;
                        mem.Path = null;
                        break;
                    }

                    break;
                }
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void MoveNode(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine("\nCan't move node while the path animation is running.");
                    return;
                }
                foreach (var node in mem.ActiveCity.Nodes.Where(node => node.Name == args[1]))
                {
                    int x, y;
                    if (int.TryParse(args[2], out x) && int.TryParse(args[3], out y))
                    {
                        node.Coords.X = x;
                        node.Coords.Y = y;
                        mem.Running = false;
                        mem.Path = null;
                        mem.ActiveCity.ActiveAmbulance.Coords =
                            new Vector2(mem.ActiveCity.ActiveAmbulance.CurrentNode.Coords.X,
                                mem.ActiveCity.ActiveAmbulance.CurrentNode.Coords.Y);
                        mem.RunningAmbulance.Coords = new Vector2(mem.ActiveCity.ActiveAmbulance.Coords.X,
                            mem.ActiveCity.ActiveAmbulance.Coords.Y);
                        mem.ActiveCity.RecalculateEdgeWeights();
                    }
                    else
                    {
                        Console.WriteLine("\nInvalid coordinates.");
                    }

                    break;
                }
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void RemoveNode(string[] args)
        {
            if (mem.ActiveCity != null)
            {
                if (mem.Running)
                {
                    Console.WriteLine("\nCan't remove node while the path animation is running.");
                    return;
                }
                foreach (var node in mem.ActiveCity.Nodes.Where(node => node.Name == args[1]))
                {
                    if (node.Name == mem.ActiveCity.ActiveAmbulance.CurrentNode.Name)
                    {
                        Console.WriteLine("\nCan't remove node because it is the Ambulance's starting point.");
                        break;
                    }
                    foreach (var edge in node.Roads)
                    {
                        var otherSide = edge.Node2;
                        if (edge.Node2 == node)
                            otherSide = edge.Node1;

                        otherSide.RemoveEdge(edge);
                        mem.ActiveCity.Edges.Remove(edge);
                    }

                    for (var i = 0; i < mem.ActiveCity.Patients.Count;)
                    {
                        if (mem.ActiveCity.Patients[i].Target == node || mem.ActiveCity.Patients[i].Location == node)
                            mem.ActiveCity.Patients.RemoveAt(i);
                        else
                        {
                            i++;
                        }
                    }
                    mem.ActiveCity.Nodes.Remove(node);
                    mem.Running = false;
                    mem.Path = null;
                    break;
                }
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private void AddNode(string[] args)
        {
            if (mem.Running)
            {
                Console.WriteLine("\nCan't add node while the path animation is running.");
                return;
            }
            if (mem.ActiveCity != null)
            {
                var r = new Random();
                var id = r.Next();
                var contains = true;
                while (contains)
                {
                    var found = mem.ActiveCity.Nodes.Any(node => node.Id == id);
                    if (!found)
                        contains = false;
                    else
                        id = r.Next();
                }
                var type = Node.NodeType.REGULAR;
                if (args[4] == "pump")
                    type = Node.NodeType.PUMP;
                else if (args[4] == "hospital")
                    type = Node.NodeType.HOSPITAL;
                else if (args[4] != "regular")
                {
                    Console.WriteLine("\nInvalid node type.");
                    return;
                }
                int x, y;

                if (!int.TryParse(args[2], out x) || !int.TryParse(args[3], out y))
                {
                    Console.WriteLine("\nInvalid coordinates.");
                    return;
                }

                int capacity = int.MaxValue;
                if (args.Length >= 6)
                {
                    if (!int.TryParse(args[5], out capacity))
                    {
                        Console.WriteLine("\nInvalid capacity.");
                        return;
                    }
                }
                var repeatedName = mem.ActiveCity.Nodes.Any(node => node.Name == args[1]);

                if (repeatedName)
                {
                    Console.WriteLine("\nMust specify a unique name.");
                    return;
                }

                mem.ActiveCity.Nodes.Add(new Node(args[1], id, type, x, y, capacity));
            }
            else
            {
                Console.WriteLine("\nMust load a map first.");
            }
        }

        private static string[] Split(string stringToSplit, params char[] delimiters)
        {
            var results = new List<string>();

            var inQuote = false;
            var currentToken = new StringBuilder();
            foreach (var currentCharacter in stringToSplit)
            {
                if (currentCharacter == '"')
                {
                    // When we see a ", we need to decide whether we are
                    // at the start or send of a quoted section...
                    inQuote = !inQuote;
                }
                else if (delimiters.Contains(currentCharacter) && inQuote == false)
                {
                    // We've come to the end of a token, so we find the token,
                    // trim it and add it to the collection of results...
                    var result = currentToken.ToString().Trim();
                    if (result != "") results.Add(result);

                    // We start a new token...
                    currentToken = new StringBuilder();
                }
                else
                {
                    // We've got a 'normal' character, so we add it to
                    // the curent token...
                    currentToken.Append(currentCharacter);
                }
            }

            // We've come to the end of the string, so we add the last token...
            var lastResult = currentToken.ToString().Trim();
            if (lastResult != "") results.Add(lastResult);

            return results.ToArray();
        }
    #endregion
    }
}
