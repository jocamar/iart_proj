﻿using System;

namespace IART_proj.BaseStructures.Exceptions
{
    /// <summary>
    /// Exception thrown when the target assigned to a patient is not an hospital.
    /// </summary>
    class InvalidTargetNodeException : Exception
    {
        public override string Message
        {
            get { return "Problem occurred when assiging target to patient: not an hospital"; }
        }
    }
}
