﻿using System;

namespace IART_proj.BaseStructures.Exceptions
{
    /// <summary>
    /// Exception thrown when trying to define a node type with a name or type identifier that have already been used in another node type.
    /// </summary>
    class RepeatedNodeTypeException : Exception
    {
        private readonly string _type;

        public RepeatedNodeTypeException(string type)
        {
            this._type = type;
        }

        public override string Message
        {
            get { return "Problem defining new node type: type identifier (" + _type + " already exists."; }
        }
    }
}
