﻿using System;

namespace IART_proj.BaseStructures.Exceptions
{
    /// <summary>
    /// Exception thrown when an edge can't be added to a node due to the RoadConnection already being occupied or being from an invalid type
    /// for the given road.
    /// </summary>
    class InvalidRoadException : Exception
    {
        private int id;

        public InvalidRoadException(int id)
        {
            this.id = id;
        }
        public override string Message
        {
            get { return "Problem occurred when adding edge to node " + id + "."; }
        }
    }
}
