﻿using System;
using System.ComponentModel;

namespace IART_proj.BaseStructures.Exceptions
{
    class RepeatedIdException : Exception
    {
        private readonly string location;
        private readonly int id;
        public RepeatedIdException(string location, int id)
        {
            this.location = location;
            this.id = id;
        }

        public override string Message
        {
            get { return "Repeated id of " + location + ": " + id; }
        }
    }
}
