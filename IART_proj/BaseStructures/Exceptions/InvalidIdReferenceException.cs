﻿using System;
using System.Globalization;

namespace IART_proj.BaseStructures.Exceptions
{
    class InvalidIdReferenceException : Exception
    {
        private readonly string location;
        private readonly string id;
        public InvalidIdReferenceException(string location, int id)
        {
            this.location = location;
            this.id = id.ToString(CultureInfo.InvariantCulture);
        }

        public override string Message
        {
            get { return "Invalid Id reference on " + location + ": " + id; }
        }
    }
}
