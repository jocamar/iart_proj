﻿#region Using Statements
using System;
using System.Threading;
using IART_proj.BaseStructures;
using IART_proj.BaseStructures.UI;

#endregion

namespace IART_proj
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var mem = new SharedMemory();
            using (var gui = new PatientTransportSimulation(mem))
            {
                var controller = new ConsoleController(mem);
                var controllerThread = new Thread(controller.Run);

                controllerThread.Start();

                while (!controllerThread.IsAlive)
                {
                }
                gui.Run();
                controllerThread.Abort();
                controllerThread.Join();

                Console.WriteLine("\nProgram exiting. Have a good day.");
            }
                
        }
    }
#endif
}
